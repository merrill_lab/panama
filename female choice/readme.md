## Data for female choice experiments: Panama 2018-19.
Chi-Yun Kuo & Richard Merrill

### 1) Behavioural data (see methods below)

#### A) Raw behavioural data from trials. 

Time stamped observations of male and female behaviours (see below), for 120 mins (starting from release of female), or until mating occurs. 

Definition of male and female behaviors follow Southcott and Kronforst 2018. Ethology 124: 862-869:

Male behaviors:

1. Chase: male follows female closely while both are flying
2. Court: male hovers over perched female
3. Mate attempt: male lands next to female and bends abdomen towards hers 

Female behaviors:

1. Close wings: perched female holds wings closed
2. Open wings: perched female opens wings and holds them there, with abdomen raised upwards. 
3. Flutter: perched female rapidly opens and closes wings while lifting abdomen and, usually, exposing abdominal scent glands
4. Fly: female flies away from male (including taking off from perched position
 

Raw behavioral data files are named following the format of: species_female insectary id_male insectary id_experiment date (e.g. MP_728_711_07282019).


#### B) 2) Summary female choice trials, cp_female_choice.csv and mp_female_choice.csv

Each file contains the following variables, each of which occupies a column

exp\_type: e.g. *melpomene* or *cydno*
 
date = Date female introduced (i.e. DAY 1)

female_id

female_age = Female age on day introduced to experimental cage (DAY 1), i.e. days since eclosion
 
male_id

male_age = Male age on day introduced to experimental cage (DAY 1),  i.e. days since eclosion

male_treatment = "white" in CP and "red" in MP mean that male color was unaltered by being painted over with a colorless marker; "red" in CP and "black" in MP meant that the male forewing bands were painted over using markers of indicated color.

mating_outcome = 0 means mating did not occur and 1 means mating occurred.

num_close_wing: number of "close wing" behavior observed

num_open_wing: number of "open wing" behavior observed

num_flutter: number of "flutter" behavior observed

num_fly: number of "fly" behavior observed

num_chase: number of "chase" behavior observed

num_court: number of "court" behavior observed

num_mate_attempt: number of "mate attempt" behavior observed

total_male_behavior: num_chase + num_court + num_mate_attempt

latency_chase: time elapsed from the beginning of the experiment until the occurrence of the first chase behavior, estimated to the nearest minute

latency_court: time elapsed from the beginning of the experiment until the occurrence of the first court behavior, estimated to the nearest minute

latency_mate_attempt: time elapsed from the beginning of the experiment until the occurrence of the first mate attempt behavior, estimated to the nearest minute

total_observation_time: total amount of time with which the researchers monitor mating between the focal pair, recorded to the nearest minute. 2880 (=  48 hours)unless mating occurs.


### 2) Image analysis data (see methods below)

Cone catch model data (*I'm not sure this is raw data we collect, but we should record the raw data values in this repository*) for Copic Ciao markers R05, R14, R17, R27, R29, R35, R46, RV29, *colourless* applied to *H. cydno chionius* white forewing band.

Cone catch model data for *H. cydno chionius*, *H. melpomene rosina*, (red/white) forewing band and *H. melpomene rosina* forewing black region. 

##  General methods

### 3)Female choice trials: 

####DAY 0 (*i.e.* day of female eclosion)
i) Male (*>= 10 days*) randomly assigned one of two colour treatments: control (colourless marker), or experimental (pigmented marker). 

ii) Male forewing band are  painted with pigmented (experimental treatment) or colourless (control) Copic marker. 

iii) Males moved to an experimental cage (*dimensions*) to acclimatise overnight. 

####DAY 1
i) Focal females (1 day old virgins, i.e. day after eclosion) introduced to the experimental cage between *time*, after 10 minute acclimatisation in a small 'pop-up' cage.

ii) After female introduction, behaviours are recorded continuously (at inception of a new behavior) for 2 hours, or until mating occurs.

**Behaviours recorded** (Broadly follow those of Southcott and Kronforst (2018) Ethology 214:862-869): 
 
*Male behaviours:* 1) 'Courtship': includes 'chase' (male follows female closely while both are flying), and 'court' (male hovers over perched female); 2) 'Mate attempt': Male lands next to female and bends abdomen towards hers.

*Female behaviours:* 1) 'Open wings': Perched female opens wings and holds them there; 2) 'Flutter': Perched female rapidly opens and closes wings while lifting abdomen and, usually, exposing abdominal scent glands; 3)'Close wing': Perched female holds wings closed; 4) 'Fly': Female flies away from male (including taking off from perched position).

#### DAY 1-2

i) Cage monitored every ~30-60 minutes from 8:00 am to 6:00 pm for 48 hours after introducing female (i.e. from DAY 1 until 6:00 pm on DAY 2).

*Are we certain we catch all matings? In the past I have frequently seen them mating at dusk. Let's finish the pilot data (10 experimental, 10 control for each experiment), and see if we need to watch them for more than 2 hours. I see how it is very time consuming, but given that we are currently have so few trials we should collect the best data possible* 

All male and females are only used in one trial to ensure independence.

### 4) Multispectral image analyses:

i) Tested eight different red markers tested: (Copic Ciao R05, R14, R17, R27, R29, R35, R46 and RV29, Tokyo, Japan) 

ii) Multispectral image analyses developed by Troscianko and Stevens (2015) in ImageJ (Schneider et al. 2012):

- first coloured eight distinct white areas on a H. cydno FORE-wing (*did we repeat this* No, we did not.) with each of the marker. We took photographs of the colored H. cydno wing and a H. m. rosina forewing with a Nikon Nikkor D7000 camera (Nikon, Melville NY, USA) *and a Nikkor 105mm lens* with a visible light (range allowed) or a UV (range allowed) filter (*company, city*). 

- A 40% grey standard (*company, city*) was included in each photograph for color calibration. 

-The visible light and UV photos of each wing were combined to generate a multispectral image, which the software then converted to cone catch models based on the visual sensitivity and relative abundance of cone receptors for species in the H. melpomene clade (McCulloch et al. 2017; Parnell et al. 2018). 

- Based on the cone catch model results, we calculated pairwise just noticeable differences (JND) between the red forewing band in H. melpomene rosina and each of the eight colored areas on the H. cydno wing, using a Weber fraction of 0.05. A JND value less than 2 is conventionally considered as indistinguishable by the visual system (Trosciano and Stevens 2015). The marker R27 (Cadmium Red) had the lowest pairwise JND (1.12) and was therefore the marker we used to alter the forewing colors in experimental H. cydno males.