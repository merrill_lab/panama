# Panama data 

For depositing i) behavioural data and ii) sample and brood data collected in Panama from September 2017 as well as iii) code for summarising data and analysis 

## n.b.

# KEEP ALL DATES IN YYYY-MM-DD FORMAT

## `CP` = *Heliconius cydno chionius* (from Panama)
## `MP` = *Heliconius melpomene rosina* (from Panama)
## `HC` = *Heliconius huerippa* (from Colombia)
## `TLC` = *Heliconius timareta lineresi* (from Colombia)
## `TTPu` = *Heliconius timareta thelxinoe* (from Peru)
## `MAPu` = *Heliconius melpomene amarylis* (from Peru)

#### Includes three main directories:

### 1) `panama_samples`, includes:

####  `panama_broods` and `panama_samples`
as described in **`processing_and_recording_broods.md`** (also included)

### 2) `raw_data`, includes:

- `male_pref_raw` csv file with data collected directly from male preference supercut.

**`date`** = video collection date, in YYYY-MM-DD format; **`observer`** = who scored the video; **`red_Y_N`** = presence of red on forewing, either `Y` = red present no white/yellow, `N` = red absent, white/yellow present, `M` = "mixed", both red and white/yellow present on forwing; **`left_mark`** and **`right_mark`** = number of marks on left and right dorsal surface of wing (`0`, `1`, `2` or `3`) used to identify individuals in video; **`insectary_ID`** = insectary id of  male as described in `processing_and_recording_broods.md`; **`start_Time`** = time recorded behaviour starts; **`duration_frames`** = number of frames male associated with model; **`behavior`** = `FB`(fly-by: male flies through the field of view without exhibiting any associative bahavior toward the model), `FT`(when a male exhibits one of the following behaviours: i) changing his flight trajectory to turn toward the model, ii) flying towards the model then turning sharply away after approaching, iii) quickly turning towards and away from the model during an otherwise linear flight trajectiry, iv) circling around the model once) or `H` (hovering). **`model`** = type of model in Panama `Mel` = *H. melpomene rosina* or `Cydno` = *H. cydno chionius*; **`model_id`** = individual id for model ; **`camera_position`** = location of camera within experimental preference cage (1-6). Each number denotes one of the six locations in which we can place a model; **`video_id`** = denotes in which original video (1, 2, 3 or 4) was the observation from (only relevant for making the supercut); **`camera_ID`** = which camera is used for recording (A or B).

- `males_in_pref_exp.csv`, csv file recording id and dates males go in and out of male preference experimental cage.

**`insectary_ID`** =  as described in `processing_and_recording_broods.md`;
**`type`** =  as described in `processing_and_recording_broods.md`; **`LMU_ID`** = as described in `processing_and_recording_broods.md`; **`brood`** = as described in `processing_and_recording_broods.md`; **`red_Y_N`** = as described above; **`left_mark`** and **`right_mark`** = as described above; **`date_eclosed`** = date male emerged from pupae in YYYY-MM-DD format; **`date_in_cage`** = date into experimental cage in YYYY-MM-DD format; **`date_out_cage`** date out of experimental cage out in YYYY-MM-DD format; **`outcome`** of male `survived` until removed from cage or `died`; **`notes`**

### 3) `data_summary`

Includes summary of `raw_data` files produced by `male.pref_summarise.R`

`panama_pref_complete.csv` is a larger version of `panama_pref.csv`

as described within `../male.pref_summarise.R`

#### as well as ...

### 4) `anal_code`

*Chi - should we include these elsewhere to keep this directory tidy?*

- pref_funnelplot.csv: organized data sorted by individuals and contains information on the number of associations of each individual to either model and the preference for each individual calculated as the proportion of total associations with the MP model

- prelim_analyses.R: R code for prelimary data analyses, including the code for producing pref_funnelplot.csv from raw data, for producing the funnel plot, and for visualizing the preference vs. number of total associations curve for each individual
