## Processing, recording and preserving individuals and broods ##

Merrill, December 2017

Instructions for recording and preserving individuals in the insectary and the field. Although there is redundancy in the data collected for individuals, this is useful so that data can be cross checked later when discrepancies appear.

### Insectary numbers

All individuals that are 1) used in experiments, 2) parents of genetic crosses (broods), and/or 3) the offspring of genetic crosses are given an "insectary number", so that they can easily be identified. In most cases individuals should be assigned insectary numbers as soon as their wings are hard enough to be written on with a sharpie. **Numbers must be written clearly** so others can read them and on the ventral side of one wing (i.e. the duller underside). Try not to get fingerprints on the butterfly wings, or otherwise damage them, when possible handle with flat tweezers.

Because it is sometimes difficult to write more than three digits on a butterfly wing, we normally use 001 through 999, before starting from 0 again. Assuming that no two butterflies with the same insectary number are alive at the same time this should cause no problem.

Insectary numbers and related information should be recorded on data sheets in hard copy and kept in the insectaries. This is primarily intended as a record for reference in the insectaries, and to obtain relevant information when preserving butterflies and/or recording individual information relating to experiments. It is not *necessarily* important to have a digital copy (i.e. .csv file or similar) but scans need to be taken of the insectary numbers at regular intervals, and pdf files saved in an organised way so that these can later be cross referenced (even with the most careful data recording there will be discrepancies that need to be checked). It's normally a good idea to make data sheets with insectary numbers running well into the future, as this avoids accidentally assigning the same number to two individuals.

The insectary number data sheets should include:

`Insectary_number	Type	Sex	Brood	Date_eclosed	Date_died(preserved)	Mother_of	Notes`

where ...

`Type` = species or hybrid type: e.g., a pure *cydno* would be `"CP"`, an F1 hybrid with a *cydno* mother and *melpomene* father would be `"CPxMP"`, and a backcross (to cydno) hybrid would be `"CPx(CPxMP)"` etc. (See below for notation.)

`Sex` = **M**ale or **F**emale

`Brood` = Which brood does the individual belong to. If individuals are caught in the wild state this (preferably with a note of the collecting location).

`Date_eclosed` = YYYY-MM-DD

`Date_died(preserved)` = YYYY-MM-DD (**Y**es/**N**o if individual preserved for DNA extraction)

`Mother_of` = Brood number that individual is a mother of. e.g. *cydno* female is mated to a *melpomene* male, her offspring belong to brood X, so she is mother of brood X.

`Notes` = *any* relevant information.


**Notation:** It is convention for genetic crosses that the female type is written first; so `"CPxMP"` indicates a cross between a *cydno* female (from Panama) and a *melpomene* male (from Panama), and `"MPxCP"` indicates a cross between a *melpomene* female (from Panama) and a *cydno* male (from Panama). Similarly, `"CPx(CPxMP)"` would indicate a cross between a *cydno* mother and an F1 (`CPxMP`) father, and `"(CPxMP)x(CPxMP)"` would indicate an intercross etc. etc. Note that: `(CPxMP)x(CPxMP) ≠ (MPxCP)x(CPxMP) ≠ (MPxCP)x(MPxCP)`, etc. 

Following our previous work:
`CP` = *Heliconius cydno chionius* (from Panama);
`MP` = *Heliconius melpomene rosina* (from Panama);
... and then:
`HC` = *Heliconius huerippa* (from Colombia);
`TLC` = *Heliconius timareta lineresi* (from Colombia);
`TTPu` = *Heliconius timareta thelxinoe* (from Peru);
`MAPu` = *Heliconius melpomene amarylis* (from Peru);

notation for other species/populations can be added, but must be consistent.



### Recording genetic crosses/broods ##

It is *very* important that both the parents of genetic crosses (broods) and other  related information is accurately recorded at the time of mating.

In most cases we will be crossing either two pure individuals, e.g. `CP` and `MP` (= an "interspecific cross") or a pure female, e.g. a `CP`, with a F1 hybrid male, e.g. `CPxMP`, (= a "backcross"); the first situation would result in an F1 brood (`CPxMP`), and the second a backcross brood (`CPx(CPxMP)`). [*Other crosses are possible, but are probably less useful to us.*] In both cases the female must be a virgin, and will normally be fairly freshly eclosed. With more difficult crosses, individuals can be hand-paired or pupae can be put into cages with adult males. In the latter case females can be assigned an insectary number after they have mated, and separated.

If other individuals are also in the cage, the mating pair must either i) be moved somehere else (very carefully so as not to serperate them), or ii) the insectary number of both individuals recorded. Either way it is very important that we know the identity of *both* individuals. 

**As soon as the mating pair have separated (at least >30 mins) the father must be removed and preserved immediately for DNA extraction (see below).**

The **mother** should be assigned to the next available (unique) brood number - all of her offspring will belong to this brood. [*In Panama: While we are working with Chris' CPxMP crosses I suggest we adopt the brood naming system that Diana is currently using. When we start crosses between timereta and cydno I suggest below system (though I'm open to suggestions):*]

**Brood names should include the insectary location, year and a number**. So for example: `P18_1` would be the first brood from the Panama insectaries for 2018, and `C19_3` would be the third brood from the Colombia insectaries for 2019.

[*Does that make sense? Is this a good system?*] It often is a good idea to write the brood name (perhaps reduced from `P18_1` to `P1`, or `C19_3` to `C3`) on the mother's wing (so that the insectary number is on the ventral side of one wing and the brood number is on the ventral side of the other). It is again very important that we do not loose brood mothers; they need to be checked multiple times a day. If they start to look sickly, stop laying eggs (unless it's very rainy/hot) etc. sacrifice her. It is very hard to extract sufficient DNA from already dead butterflies - and more likely than not ants will very quickly destroy the tissue. Similarly, ensure there is no risk of being eaten by geckos etc. 

Any uncertainty about the identification of brood parents makes downstream analysis very difficult. So **new broods must be recorded immediately**: Both on a data sheet in the insectary (for reference) *and* in the electronic form. [*I will make a csv file for the bitbucket repository for now, but I'm hoping to set up a more permanent data base. Hopefully we can get it up and running asap. Any input would be very welcome*]. Brood recordings should include:

`Brood	Cross_type	Date_achieved	Mother_insectary_num	Mother_sample	Mother_type	Mother_brood Father_insectary_num	Father_sample	Father_type		Father_brood	notes`

where ...

`Brood` = as above e.g. `P18_1`, `P18_2`, `P18_3` etc.

`Cross_type` = mother type x father type, e.g. for a interspecific cross between a *cydno* female and a *melpomene* male = `CPxMP`

`Date_achieved` =  date parents mated YYYY-MM-DD

`Mother_insectary_num` = mother's insectary number

`Mother_sample` = sample number given when preserved (see below)

`Mother_type` = species or hybrid type, e.g. for interspecific example cross (as above) between a *cydno* female and a *melpomene* male = `CP`

`Mother_brood` = only applicable when the mother is a hybrid (normally F1), brood he is derived from e.g. `P18_2`, if not 'wild' or 'stock'

`Father_insectary_num` = father's insectary number

`Father_sample` = sample number given when preserved (see below)

`Father_type` = species or hybrid type, e.g. for interspecific example cross (as above) between a *cydno* female and a *melpomene* male = `MP`

`Father_brood` = only applicable when the father is a hybrid (normally F1), brood he is derived from e.g. `P18_2`, if not 'wild' or 'stock'

`notes` = *any* relevant information

***

### Preserving individuals ##

**Any individuals from which we may want to extract DNA for genetic analysis need to be preserved in 'DMSO' () and stored at -20 (or better -80) as soon as possible.** It is often very difficult to extract sufficient DNA from already dead butterflies. (But if butterflies are found dead they should be sampled as soon as possible, with a note on their condition.) This is of course very important for brood parents, the alleles introduced into a cross come from the parents and it is harder (though not impossible) to determine the segregation of alleles from parental species without their genetic material. It is important to sample as many individuals from a brood as possible, even if there is no interesting phenotypic data, because it i) makes it easier to determine the segregation of parental alleles, and ii) these individuals may be useful for unforeseen analyses in the future. This is especially helpful if there are missing parents.  

Individuals to be preserved are assigned a unique sample number. [*In Panama: While we are working with Chris' CPxMP crosses I again suggest we adopt the CAM_IDs,  that Diana is already using. When we start crosses between timereta and cydno I suggest this we start our own `LMU_ID ...` series running from `LMU_00001` through to `LMU_99999` (I'm ambitious!)*]. This sample number *relates to the individual*, so if different tissue are collected from the same individual they should have the same sample number (but each tissue type clearly labeled), e.g. if we collect body tissue in DMSO for DNA analyses, brain tissue n RNAlater for expression analyses and wings for an individual assigned `LMU_00012`, both (DMSO and RNAlater) tubes and wing envelope should be labelled with `LMU_00012`. LMU_ID sample numbers are universal across the lab – we all contribe to the same collection so that they can easily be found by anyone.

**Individual data must be recorded immediately for each butterfly sampled** in electronic form. [*Again I will make a csv file for the bitbucket repository for now, but I'm hoping to set up a more permanent data base. We need a system where we can reserve sample numbers so that, for example we can reserve LMU_00001 - LMU_02000 for Chi in Panama and LMU_02001 - LMU_04000 for Alex and Marilia in Colombia; i.e. so it is impossible to use the same numbers. Again, input would be welcome*]. This should include:

`Sample	Insectary_number Sex	Assigned	Type	Brood	Mother_of	Father_of	Date_eclosed	Date_sampled	Sample_location	Collection_location Collection_data	DNA	Wings	Other	Notes`

[*Should we add other information*]

where ...

`LMU_ID` = Is the unique LMU sample number (see below)

`Insectary_number` = insectary number see above [*It might be worth prefixing this with the year, e.g. `18_089`?*)

`Sex` = `M` or `F` 

`Assigned` = Sample number reserved by research X, so that in the field/insectary the same `LMU_ID` is not used by two people. [*Probably a better way to do this, but it's useful to reserve sample numbers, if for example we are working without easy access to. Also potentially allows us to print of labels in advance. Ideas?*] 

`Type` = individual's species or hybrid type: so a pure *cydno* would be `"CP"`, an F1 hybrid with a *cydno* mother and *melpomene* father would be `"CPxMP"`, and a backcross (to cydno) hybrid would be `"CPx(CPxMP)"` etc. (See above for notation.)

`Brood` = mother's insectary number, or if wild state that

`Mother_of`	= if the individual is a brood mother state the brood

`Father_of` = if the individual is a brood father state the brood

`Date_eclosed` = YYYY-MM-DD

`Date_sampled` = YYYY-MM-DD

`Sample_location` = where is the sample, i.e. which freezer and position [*How to do this may require some thought, but it will be very useful later*]

`Collection_location` = for wild caught individuals	

`Collection_data` = YYYY-MM-DD for wild caught individuals	

`DNA` = Tissue preserved for DNA **Y**es or **N**o 

`Wings` = Wings preserved in an envelope **Y**es or **N**o 

`Other` = what else preserved (e.g. brain_RNA)

`Notes` = *any* relevant information




For all individuals, the wing must be pulled off with tweezers (or cut off) and placed in a glassine envelope, and the body in a 2ml tube of DMSO. It is actually better to 'pluck' the wings off with 'flat' tweezers as this often ensures a better sampled wing. If they are cut it needs to be close to the body to preserve as much of the pattern as possible. 

Both the tube and the envelope should be labelled with the date and the sample number [*I will look into label printing as this reduces mistakes, and 'double numbering' in particular*]. The wing envelope should  be labelled with additional information, including **type** (e.g. F1 `CPxMP`), **if individual is a brood parent** (e.g. 'father of brood `P18_3`' - ensure it is clear that the individual is parent of that brood, and not from that brood), **the individual's own brood** (e.g. 'brood `P18_1`') etc. The more information recorded on the envelope the better (as long as the sample number remains clear).

Wing envelopes should be stored in order in a *tupperware* box in dry place. Tubes with bodies in DMSO should be stored at -20 or (preferably) -80, in order. 

Killing butterflies must be done properly so that they are useful for research: I do not want to kill butterflies for no reason. It might be considered kinder to chill the butterflies at -20 for a *few* minutes first, but I don't know how this effects DNA quality etc. Butterflies from the insectaries should **never** be released into the wild.